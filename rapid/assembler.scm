;;; Rapid Scheme --- An implementation of R7RS

;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-record-type <arch>
  (make-arch msb?)
  arch?
  (msb? arch-msb?))

(define-record-type <assembler>
  (%make-assembler sections current-section imports exports labels patches)
  assembler?
  (sections assembler-sections)
  (current-section assembler-current-section
		   assembler-set-current-section!)
  (imports assembler-imports assembler-set-imports!)
  (exports assembler-exports assembler-set-exports!)
  (labels assembler-labels)
  (patches assembler-patches assembler-set-patches!))

(define (make-assembler msb?)
  (let ((text-section (make-section msb?)))
    (%make-assembler `((text . ,text-section)
		       (data . ,(make-section msb?))
		       (bss . ,(make-section msb?)))
		     text-section
		     (make-hash-table symbol-comparator)
		     '()
		     (make-hash-table symbol-comparator)
		     '())))

(define (assembler-section assembler name)
  (cdr (assq name (assembler-sections assembler))))

(define (assembler-current-offset assembler)
  (section-tell (assembler-current-section assembler)))

(define (assembler-add-export! assembler label)
  (assembler-set-exports! assembler
			  (cons label (assembler-exports assembler))))

(define (assembler-get-object assembler)
  (for-each (lambda (patch)
	      (assembler-process-patch assembler patch))
	    (assembler-patches assembler))
  (let ((imports
	 (hash-table-values (assembler-imports assembler)))
	(exports
	 (map (lambda (export)
		(let ((label (assembler-label assembler export)))
		  (make-export export
			       (label-section label)
			       (label-offset label))))
	      (assembler-exports assembler))))
    (make-object-module (assembler-section assembler 'text)
			(assembler-section assembler 'data)
			(assembler-section assembler 'bss)
			exports
			imports)))

(define (assemble source arch)
  (let ((assembler (make-assembler (arch-msb? arch))))
    (for-each (lambda (source-line)
		(assemble-line source-line assembler arch))
	      source)
    (assembler-get-object assembler)))

(define (assemble-line source-line assembler arch)
  (define (immediate operand) (assemble-immediate assembler operand))
  (match source-line
    ;; call arch
    (,label
     (guard (symbol? label))
     (assembler-add-label! assembler label))
    ((.text)
     (assembler-set-current-section! assembler
				     (assembler-section assembler 'text)))
    ((.data)
     (assembler-set-current-section! assembler
				     (assembler-section assembler 'data)))
    ((.bss)
     (assembler-set-current-section! assembler
				     (assembler-section assembler 'bss)))
    ((.global ,label) (assembler-add-export! assembler label))
    ((.align ,size) (assembler-align! assembler size))
    ((.ascii ,str* ...) (assembler-emit-strings assembler str* #f))
    ((.asciz ,str* ...) (assembler-emit-strings assembler str* #t))
    ((.byte ,(immediate -> imm*) ...)
     (assembler-emit-data assembler imm* 1))
    ((.word ,(immediate -> imm*) ...)
     (assembler-emit-data assembler imm* 2))
    ((.long ,(immediate -> imm*) ...)
     (assembler-emit-data assembler imm* 4))
    ((.quad ,(immediate -> imm*) ...)
     (assembler-emit-data assembler imm* 8))
    (,x (error "assemble-line: invalid line" x))))

(define (assembler-align! assembler size)
  (section-align! (assembler-current-section assembler) size)
  (let ((pos (assembler-current-offset assembler)))
    (section-write-bytevector (assembler-current-section assembler)
			      (make-bytevector (modulo (- pos) size) 0))))

(define (assemble-immediate assembler operand)
  (match operand
    (,number
     (guard (integer? number))
     number)
    (,label
     (guard (symbol? label))
     (make-offset (assembler-label assembler label) 0))
    ((+ ,label ,addend)
     (guard (and (symbol? label) (integer? addend)))
     (make-offset (assembler-label assembler label) addend))
    (,x (error "assemble-immediate: invalid immediate" x))))

(define (assembler-add-label! assembler name)
  (let ((label (assembler-label assembler name)))
    (label-set-section! label (assembler-current-section assembler))
    (label-set-offset! label (assembler-current-offset assembler))))

(define (assembler-import assembler name)
  (hash-table-intern! (assembler-imports assembler)
		      name
		      (lambda ()
			(make-import name))))

(define (assembler-emit-data assembler imms size)
  (for-each
   (lambda (imm)
     (assembler-emit-immediate assembler #f imm size))
   imms))

(define (assembler-emit-strings assembler strings null?)
  (for-each
   (lambda (string)
     (string-for-each (lambda (char)
			(section-write-u8 (assembler-current-section assembler)
					  (char->integer char)))
		      string)
     (when null?
       (section-write-u8 (assembler-current-section assembler)
			 0)))
   strings))

(define (assembler-emit-immediate assembler relative? value size)
  (cond
   ((integer? value)
    (section-write-integer (assembler-current-section assembler) value size))
   (else
    (assembler-add-patch! assembler
			  relative?
			  (offset-addend value)
			  (assembler-current-section assembler)
			  (assembler-current-offset assembler)
			  (offset-label value)
			  size)
    (section-write-integer (assembler-current-section assembler) 0 size))))

;;; Offsets

(define-record-type <offset>
  (make-offset label addend)
  offset?
  (label offset-label)
  (addend offset-addend))

;;; Labels

(define-record-type <label>
  (%make-label name section offset)
  label?
  (name label-name)
  (section label-section label-set-section!)
  (offset label-offset label-set-offset!))

(define (make-label name)
  (%make-label name #f #f))

(define symbol-comparator
  (make-comparator symbol? eq? #f symbol-hash))

(define (assembler-label assembler name)
  (hash-table-intern! (assembler-labels assembler)
		      name
		      (lambda ()
			(make-label name))))

;;; Patches

(define-record-type <patch>
  (make-patch relative? addend section offset label size)
  patch?
  (relative? patch-relative?)
  (addend patch-addend)
  (section patch-section)
  (offset patch-offset)
  (label patch-label)
  (size patch-size))

(define (assembler-add-patch! assembler
			      relative? addend section offset label size)
  (assembler-set-patches! assembler
			  (cons (make-patch relative?
					    addend
					    section
					    offset
					    label
					    size)
				(assembler-patches assembler))))

(define (assembler-process-patch assembler patch)
  (let ((label (patch-label patch)))
    (cond
     ((not (label-section label))
      (import-add-relocation! (assembler-import assembler (label-name label))
			      (patch-relative? patch)
			      (patch-addend patch)
			      (patch-section patch)
			      (patch-offset patch)
			      (patch-size patch)))
     ((eq? (patch-section patch) (label-section label))
      (let* ((section (patch-section patch))
	     (pos (section-tell section)))
	(section-seek! section (patch-offset patch))
	(section-write-integer section
			       (+ (label-offset label)
				  (patch-addend patch)
				  (if (patch-relative? patch)
				      (- (patch-offset patch))
				      0))
			       (patch-size patch))))
     (else
      (section-add-relocation! (label-section label)
			       (patch-relative? patch)
			       (+ (patch-addend patch)
				  (label-offset label))
			       (patch-section patch)
			       (patch-offset patch)
			       (patch-size patch))))))
